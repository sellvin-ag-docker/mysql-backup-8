FROM ubuntu:20.04

RUN apt-get update
RUN apt-get install -y wget gnupg2 lsb-release curl

RUN wget https://repo.percona.com/apt/percona-release_latest.$(lsb_release -sc)_all.deb

RUN dpkg -i percona-release_latest.$(lsb_release -sc)_all.deb

RUN apt-get update

RUN percona-release enable-only tools release

RUN apt-get update

RUN apt-get install -y percona-xtrabackup-80

RUN apt-get install -y qpress

CMD ["/bin/bash"]